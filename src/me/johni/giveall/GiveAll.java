package me.johni.giveall;

import me.johni.giveall.commands.GiveAllCommand;
import me.johni.giveall.utils.ConfigManager;
import me.johni.giveall.utils.KeyManager;
import org.bukkit.plugin.java.JavaPlugin;

public class GiveAll extends JavaPlugin {

    private ConfigManager configManager;
    private KeyManager keyManager;

    @Override
    public void onEnable() {
        configManager = new ConfigManager(this);
        keyManager = new KeyManager(this);
        new GiveAllCommand(this);
    }

    @Override
    public void onDisable() {

    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }
}
