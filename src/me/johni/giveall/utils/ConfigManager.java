package me.johni.giveall.utils;

import me.johni.giveall.GiveAll;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {

    private GiveAll instance;

    public File file = null;

    public ConfigManager(GiveAll instance){
        this.instance = instance;
        initConfig();
    }

    public void initConfig(){
        file = new File(instance.getDataFolder() + "/config.crisis");
        FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
        }

        if(!file.exists()){
            try {
                file.createNewFile();
                configuration.createSection("Settings");
                configuration.getConfigurationSection("Settings").set("GiveCommand1", "say 1 %Player%");
                configuration.getConfigurationSection("Settings").set("GiveCommand2", "say 2 %Player%");
                configuration.getConfigurationSection("Settings").set("GiveCommand2", "say 3 %Player%");
                configuration.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    public String getKeyCommand(int type){
        file = new File(instance.getDataFolder() + "/config.crisis");
        if(file.exists()){
            FileConfiguration configuration = YamlConfiguration.loadConfiguration(file);
            return configuration.getConfigurationSection("Settings").getString("GiveCommand" + type);
        }else{
            return "File Missing";
        }
    }
}
