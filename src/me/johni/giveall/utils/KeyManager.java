package me.johni.giveall.utils;

import me.johni.giveall.GiveAll;
import org.apache.commons.lang3.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class KeyManager {

    private GiveAll instance;

    public KeyManager(GiveAll instance){
        this.instance = instance;
    }
    public int decideKeyType(){
        int random = RandomUtils.nextInt(1, 100);
        if(random <= 15){
            return 1;
        }else if (random <= 45){
            return 2;
        }else if(random <= 100){
            return 3;
        }
        return random;
    }

    public void giveKey(Player player, int type, int amount){
        String command = instance.getConfigManager().getKeyCommand(type);
        if(command.equals("File Missing")){
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "GiveALL Failed : Check Console");
        }else{
            for(int i = 0; i < amount; i++){
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),command.replace("%Player%", player.getName()));
            }

        }
    }
}
