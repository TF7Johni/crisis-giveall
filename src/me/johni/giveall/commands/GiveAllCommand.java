package me.johni.giveall.commands;

import me.johni.giveall.GiveAll;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GiveAllCommand implements CommandExecutor {

    private GiveAll instance;

    public GiveAllCommand(GiveAll instance) {
        this.instance = instance;

        instance.getCommand("giveall").setExecutor(this);
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.hasPermission("crisis.key.giveall")) {
                if (args.length <= 1) {
                    if (args.length == 0) {
                        for (Player all : Bukkit.getOnlinePlayers()) {
                            instance.getKeyManager().giveKey(all, instance.getKeyManager().decideKeyType(), 1);
                        }
                    } else {
                        for (Player all : Bukkit.getOnlinePlayers()) {
                            instance.getKeyManager().giveKey(all, instance.getKeyManager().decideKeyType(), Integer.parseInt(args[0]));
                        }
                    }
                }else{
                    player.sendMessage(ChatColor.DARK_RED + "Something went wrong : Argument Length");
                }
            }
        }
        return false;
    }
}
